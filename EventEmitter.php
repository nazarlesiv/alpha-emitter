<?php
namespace Alpha\Emitter;

class EventEmitter {
  //Listeners array.
  private $handlers = [];

  public function __construct() {}

  public function emit($e, $v) {
    if (!is_string($e) || empty($e)) {
      return false;
    }
    if (isset($this->handlers[ $e ]) && is_array($this->handlers[ $e ])) {
      $count = count($this->handlers[ $e ]);
      //todo: Build a proper, informative event object to be passed along with the call.
      for ($i = 0; $i < $count; ++$i) {
        $this->handlers[ $e ][$i]($e, $v);
      }
    }
  }

  public function on($e, $fn) {
    //Check that the even name is a valid string.
    if (!is_string($e) || empty($e) || !is_callable($fn)) {
      return false;
    }
    if (isset($this->handlers[ $e ])) {
      $this->handlers[ $e ][] = $fn;
    } else {
      $this->handlers[ $e ] = [$fn];
    }
  }
}
